--
-- File generated with SQLiteStudio v3.2.1 on อา. มี.ค. 26 14:48:50 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    id        INTEGER       PRIMARY KEY AUTOINCREMENT
                            NOT NULL,
    name      VARCHAR (32)  NOT NULL,
    price     FLOAT         NOT NULL,
    createdAt DATETIME      NOT NULL
                            DEFAULT (datetime('now') ),
    updatedAt DATETIME      NOT NULL
                            DEFAULT (datetime('now') ),
    deletedAt DATETIME,
    image     VARCHAR (128) NOT NULL
                            DEFAULT ('no_img.png') 
);

INSERT INTO product (
                        id,
                        name,
                        price,
                        createdAt,
                        updatedAt,
                        deletedAt,
                        image
                    )
                    VALUES (
                        1,
                        'product 1',
                        200.0,
                        '2023-02-19 17:00:38',
                        '2023-03-25 18:30:37',
                        NULL,
                        '135cb465-ab44-4509-8bb9-b56c36c71ce3.png'
                    );

INSERT INTO product (
                        id,
                        name,
                        price,
                        createdAt,
                        updatedAt,
                        deletedAt,
                        image
                    )
                    VALUES (
                        2,
                        'Product 2',
                        25.0,
                        '2023-02-19 17:01:38',
                        '2023-02-19 17:01:38',
                        NULL,
                        'no_img.png'
                    );

INSERT INTO product (
                        id,
                        name,
                        price,
                        createdAt,
                        updatedAt,
                        deletedAt,
                        image
                    )
                    VALUES (
                        3,
                        'Product 3',
                        250.0,
                        '2023-03-25 11:09:22',
                        '2023-03-25 11:09:22',
                        NULL,
                        '8283ed08-a2f5-4b7b-bb41-0fb62fc48fb1.png'
                    );

INSERT INTO product (
                        id,
                        name,
                        price,
                        createdAt,
                        updatedAt,
                        deletedAt,
                        image
                    )
                    VALUES (
                        5,
                        'product 6',
                        300.0,
                        '2023-03-25 18:07:13',
                        '2023-03-25 18:07:13',
                        NULL,
                        '8a07addc-fa73-46c9-8293-475f23029972.png'
                    );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
